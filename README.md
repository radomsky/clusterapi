Hi!

To begin - download the Demo_Submit_Job.ipynb file to your computer

This Jupyter Notebook can prepare a Computation Job and run it on a Cluster, all you have to do is to describe which module you want to run and how :)


1. Follow the link https://jupyterhub.embl.de/ 

2. Login with your EMBL login pass
3. Start an instance you like, but I tested only a "Data Science".
4. You are logged in you /home/ folder, it is the same as one you arrive when SSH using Terminal.
5. On the left, check if you see the list of files and folders, if not click folder icon called "File Browser" or Ctrl+Shift+F 
6. Click icon "Upload Files" and choose *.ipynb file you downloaded.
7. Inside the JupyterHub double-click the file to start it.
8. Instructions are pretty much the same as here, click run - it will download the repository
9. Explore the code in the *.ipynb and in the folder clusterapi it created.
    
Currently I disabled possibility to push for everybody as repository is public, if you want to contribute email me - I will add you to the developers group.

Feel free to drop me a line if you have questions or suggestions 

oleksandr.radomskyi@embl.it

Oleksandr Radomskyi
Rompani Group
EMBL Rome
